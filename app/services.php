<?php

use MongoClient\Service\MongoExecutorService;
use Silex\Provider\ServiceControllerServiceProvider;
use Lalbert\Silex\Provider\MongoDBServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Monolog\Logger;

$app->register(new ServiceControllerServiceProvider());
$app->register(new MongoDBServiceProvider(), [
    'mongodb.config' => [
        'server' => $app['mongoDbServer'],
        'options' => [],
        'driverOptions' => [],
    ]
]);

$app->register(new MonologServiceProvider(), [
    'monolog.logfile' => __DIR__ . '/logs/app.log',
    'monolog.name' => 'app',
    'monolog.level' => Logger::ERROR
]);

// Custom services
$app['mongoExecutorService'] = function () use($app) {
    return new MongoExecutorService($app['mongodb'], $app['dbName']);
};