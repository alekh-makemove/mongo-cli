<?php
namespace MongoClient\Command;

use MongoClient\MongoClientException;
use MongoClient\Service\MongoExecutorService;
use MongoClient\Service\SQLParserService;
use Monolog\Logger;
use Silex\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ExecuteSqlCommand extends Command {

    /**
     * @var MongoExecutorService
     */
    private $mongoExecutorService;

    /**
     * @var Logger
     */
    private $monolog;

    /**
     * @param MongoExecutorService $mongoExecutorService
     */
    public function setServices(MongoExecutorService $mongoExecutorService, Logger $monolog) {
        $this->mongoExecutorService = $mongoExecutorService;
        $this->monolog = $monolog;
    }

    protected function configure() {
        $this
            ->setName("mongo:execute")
            ->setDescription("Execute query!");
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $helper = $this->getHelper('question');
        // Ask user for sql
        do {
            $question = new Question("Please enter sql:\n");

            $sql = $helper->ask($input, $output, $question);

            try {
                // Parse sql and generate parameters for mongodb
                $sqlParser = new SQLParserService($sql);

                $mongoQuery = $sqlParser->getMongoQuery();
                $collection = $sqlParser->getCollection();
                $projections = $sqlParser->getProjections();
                $limit = $sqlParser->getLimit();
                $offset = $sqlParser->getOffset();
                $order = $sqlParser->getOrder();

                // Execute mongodb query
                $result = $this->mongoExecutorService->executeCommandByParameters($mongoQuery, $collection, $projections, $order, $limit, $offset);

                // Show result
                foreach($result as $value) {
                    \print_r($value);
                }
            } catch (MongoClientException $ex) {
                $this->monolog->error($ex->getMessage());
                echo("Error: " . $ex->getMessage() . "\n");
            } catch(\Exception $ex) {
                $this->monolog->critical($ex->getMessage());
            }
        } while(\strtolower(\trim($sql)) != 'exit');
    }
}