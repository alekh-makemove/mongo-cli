<?php
namespace MongoClient\Helper;

class StringUtilsHelper {

    /**
     * Check is string null or empty
     *
     * @param ...$strings
     * @return bool
     */
    public static function isBlankOrNull(...$strings) {
        $result = false;
        foreach ($strings as $string) {
            if ($string === null || trim($string) === '') {
                $result = true;
                break;
            }
        }
        return $result;
    }

}