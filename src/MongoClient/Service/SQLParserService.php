<?php
namespace MongoClient\Service;

use MongoClient\Helper\StringUtilsHelper;
use MongoClient\MongoClientException as MCE;
use PHPSQLParser\PHPSQLParser;

class SQLParserService {

    private $parameters = [];

    static $operators = [
        '=' => '$eq',
        '!=' => '$ne',
        '<>' => '$ne',
        '>' => '$gt',
        '>=' => '$gte',
        '<' => '$lt',
        '<=' => '$lte'
    ];

    static $logicOperators = [
        'AND' => '$and',
        'OR' => '$or'
    ];

    /**
     * SQLParserService constructor.
     *
     * @param $sql
     */
    public function __construct($sql) {
        // Replace SKIP on OFFSET
        $sql = \str_replace('SKIP', 'OFFSET', $sql);

        $queryData = new PHPSQLParser($sql);
        $parameters = $queryData->parsed ?? [];

        $this->validateParameters($parameters);

        $this->parameters = $parameters;
    }

    /**
     * @return string|null
     * @throws MCE
     */
    public function getCollection() {
        $table = null;
        $fromData = $this->getSqlParameter('FROM');
        foreach ($fromData as $parameter) {
            if ($parameter['expr_type'] == 'table') {
                $table = $parameter['table'];
                break;
            }
        }

        if (StringUtilsHelper::isBlankOrNull($table)) {
            throw new MCE('Please enter correct sql.');
        }

        return $table;
    }

    /**
     * @return array
     * @throws MCE
     */
    public function getProjections() {
        $projections = [];
        $selectData = $this->getSqlParameter('SELECT');
        foreach ($selectData as $parameter) {
            if ($parameter['expr_type'] == 'colref' && isset($parameter['no_quotes'])) {
                $parts = $parameter['no_quotes']['parts'];
                $projections[] = [($parts[1] ?? $parts[0]) => 1];
            }
        }

        return $projections;
    }

    /**
     * @param null $whereData
     * @return array
     * @throws MCE
     */
    private function getConditionsArray($whereData = null) {
        if ($whereData == null) {
            $whereData = $this->getSqlParameter('WHERE', false);
        }
        $blocks = [];
        $i = 0;

        foreach ($whereData as $parameter) {
            switch ($parameter['expr_type']) {
                case 'colref':
                    $parts = $parameter['no_quotes']['parts'];
                    $blocks[$i]['field'] = $parts[1] ?? $parts[0];
                    break;

                case 'operator':
                    if (\in_array($parameter['base_expr'], ['AND', 'OR'])) {
                        $blocks[$i]['logicOperator'] = self::$logicOperators[$parameter['base_expr']];
                        $i++;
                    } else {
                        $blocks[$i]['operator'] = self::$operators[$parameter['base_expr']];
                    }
                    break;

                case 'const':
                    $blocks[$i]['value'] = $string = \str_replace('\'', '', $parameter['base_expr']);
                    break;

                case 'bracket_expression':
                    $blocks[$i]['sub_tree'] = $this->getConditionsArray($parameter['sub_tree']);
                    break;
            }
        }

        return $blocks;
    }

    /**
     * Generate query for mongodb by WHERE parameter.
     *
     * @param null $conditions
     * @return array|null
     */
    public function getMongoQuery($conditions = null) {
        $conditions = $conditions ?? $this->getConditionsArray();

        $prevLogicOperator = null;
        $result = $orConditions = $andConditions = [];

        $logicOr = self::$logicOperators['OR'];
        $logicAnd = self::$logicOperators['AND'];

        foreach ($conditions as $condition) {
            $field = $this->getField($condition);

            if (isset($condition['logicOperator'])) {
                if ($prevLogicOperator == null) {
                    if ($condition['logicOperator'] == $logicOr) {
                        $orConditions[] = $field;
                    } else {
                        $andConditions[$logicAnd][] = $field;
                    }
                } else {
                    if ($condition['logicOperator'] == $logicAnd) {
                        $andConditions[$logicAnd][] = $field;
                    } elseif ($condition['logicOperator'] == $logicOr) {
                        if ($prevLogicOperator == $logicOr) {
                            $orConditions[] = $field;
                        } elseif ($prevLogicOperator == $logicAnd) {
                            $andConditions[$logicAnd][] = $field;

                            $orConditions[] = $andConditions;
                            $andConditions = [];
                        }
                    }
                }
                $prevLogicOperator = $condition['logicOperator'];
            } else {
                if (empty($orConditions)) {
                    if (empty($andConditions)) {
                        $result = $field;
                    } elseif (!empty($andConditions)) {
                        $andConditions[$logicAnd][] = $field;
                        $result = $andConditions;
                    }
                } else {
                    if ($prevLogicOperator !== null) {
                        if ($prevLogicOperator == $logicAnd) {
                            $andConditions[$logicAnd][] = $field;
                        } else {
                            $orConditions[] = $field;
                        }
                    }

                    if (!empty($andConditions)) {
                        $orConditions[] = $andConditions;
                    }
                    $result[$logicOr] = $orConditions;
                }
            }
        }

        return $result;
    }

    /**
     * Get limit parameter
     *
     * @return string|null
     * @throws MCE
     */
    public function getLimit() {
        $limitData = $this->getSqlParameter('LIMIT', false);
        return $limitData['rowcount'] ?? null;
    }

    /**
     * Get skip/offset paramter
     *
     * @return string|null
     * @throws MCE
     */
    public function getOffset() {
        $limitData = $this->getSqlParameter('LIMIT', false);
        return $limitData['offset'] ?? null;
    }

    /**
     * Get order parameter
     *
     * @return array
     * @throws MCE
     */
    public function getOrder() {
        $order = [];
        $orderData = $this->getSqlParameter('ORDER', false);
        foreach ($orderData as $parameter) {
            if ($parameter['expr_type'] == 'colref' && isset($parameter['no_quotes'])) {
                $parts = $parameter['no_quotes']['parts'];
                $direction = $parameter['direction'];
                $order[($parts[1] ?? $parts[0])] = $direction == 'ASC' ? 1 : -1;
            }
        }

        return $order;
    }

    /**
     * Get field for mongodb query
     *
     * @param $parameter
     * @return array|null
     */
    private function getField($parameter) {
        $field = null;
        if (isset($parameter['field'])) {
            $field = [
                $parameter['field'] => [
                    $parameter['operator'] => $parameter['value']
                ]
            ];
        } elseif (isset($parameter['sub_tree'])) {
            $field = $this->getMongoQuery($parameter['sub_tree']);
        }

        return $field;
    }

    /**
     * Get sql parameter by key
     *
     * @param $key
     * @param bool|true $strict
     * @return array
     * @throws MCE
     */
    private function getSqlParameter($key, $strict = true) {
        if ($strict && !\array_key_exists($key, $this->parameters)) {
            throw new MCE('Please add correct query.');
        }

        return $this->parameters[$key] ?? [];
    }

    /**
     * Validate sql parameters
     *
     * @param $parameters
     * @throws MCE
     */
    private function validateParameters($parameters) {
        if (!\is_array($parameters) || empty($parameters)) {
            throw new MCE('Please add correct query.');
        }
    }

}


