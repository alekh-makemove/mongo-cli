<?php

namespace MongoClient\Service;

use MongoDB\Client;

class MongoExecutorService {

    /**
     * @var Client
     */
    private $mongoClient;

    /**
     * @var string
     */
    private $dbName;

    /**
     * MongoExecutorService constructor.
     *
     * @param $mongoClient
     * @param $dbName
     */
    public function __construct($mongoClient, $dbName) {
        $this->mongoClient = $mongoClient;
        $this->dbName = $dbName;
    }

    /**
     * Execute mongodb query with parameters.
     *
     * @param array $mongoQuery
     * @param $collection
     * @param array $projections
     * @param null $order
     * @param null $limit
     * @param null $offset
     * @return \MongoDB\Driver\Cursor
     */
    public function executeCommandByParameters(array $mongoQuery, $collection, $projections = [], $order = null, $limit = null, $offset = null) {
        $params = [];
        if(!empty($projections)) {
            $params['projection'] = $projections;
        }
        if($limit !== null) {
            $params['limit'] = \intval($limit);
        }
        if($offset !== null) {
            $params['skip'] = \intval($offset);
        }
        if($order !== null) {
            $params['sort'] = $order;
        }

        return $this->mongoClient->{$this->dbName}->$collection->find($mongoQuery, $params);
    }

}


