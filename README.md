# README #

### What is this repository for? ###

Mongodb client v.1.0

This project is a client for mongodb, which allow user to use SQL for mongodb query.
(Only SELECT command).

### Requirements ###

1. PHP >= 7.0.0
2. mongodb >= 3.0.0

### How do I get set up? ###

1. Clone this repository
2. In ./configs folder create params.php file based on params.php.dist
3. Edit params.php and add your DB parameters.
4. Run composer install in the root folder, to install all needed dependencies.
5. It is necessary to have php mongodb extension installed on your computer
6. All test are in ./tests folder. For running tests you should use phpunit (https://phpunit.de)
> php phpunit.phar --bootstrap vendor/autoload.php tests/

### Usage ###

For running client run next command in project root folder:
> php ./bin/console mongo:execute

After that you will be able to use client to generate mongodb query based on regular sql.

This version of client work with SELECT command only.

SQL structure:
> SELECT [<Projections>]

> [FROM <Target>]

> [WHERE <Condition>*]

> [ORDER BY <Fields>* [ASC| DESC] *]

> [SKIP <SkipRecords>]

> [LIMIT <MaxRecords>]