<?php
namespace Tests;

use MongoDB\Client;

class FixtureService {

    /**
     * @var Client
     */
    private $mongoClient;

    /**
     * @var array
     */
    public $fixtures = [];

    /**
     * @var string
     */
    private $dbName;

    /**
     * FixtureService constructor.
     *
     * @param $mongoClient
     * @param $dbName
     */
    public function __construct($mongoClient, $dbName) {
        $this->mongoClient = $mongoClient;
        $this->dbName = $dbName;
    }

    /**
     * Set fixtures
     */
    public function setFixtures() {
        foreach ($this->fixtures as $collections) {
            $collectionName = $collections['name'];
            $this->clearDatabase($collectionName);

            foreach ($collections['data'] as $data) {
                $this->mongoClient->{$this->dbName}->$collectionName->insertOne($data);
            }
        }
    }

    /**
     * Clear all previous test data
     *
     * @param $collection
     */
    public function clearDatabase($collection) {
        $this->mongoClient->{$this->dbName}->$collection->deleteMany([]);
    }

}