<?php
namespace Tests;

use Silex\WebTestCase;

class Base extends WebTestCase {

    /**
     * Create application
     *
     * @return mixed
     */
    public function createApplication() {
        $app = require __DIR__ . '/../app/app.php';
        require __DIR__ . '/../config/params.php';
        require __DIR__ . '/../app/services.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }

}