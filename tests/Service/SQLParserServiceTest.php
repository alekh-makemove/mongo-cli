<?php
use Tests\Base;
use MongoClient\Service\SQLParserService;

class controllersTest extends Base {

    const SQL = "SELECT firstName, lastName
          FROM users u
          WHERE u.firstName = 'Firstname' AND u.lastName = 'Lastname' OR (u.firstName = 'Firstname2' AND u.lastName = 'Lastname2')
          ORDER BY u.firstName
          DESC LIMIT 20 SKIP 1";

    public function testGetCollectionSuccess() {
        $sqlParserService = new SQLParserService(self::SQL);
        $collectionName = $sqlParserService->getCollection();

        $this->assertEquals($collectionName, 'users');
    }

    public function testGetProjectionsSuccess() {
        $sqlParserService = new SQLParserService(self::SQL);
        $projections = $sqlParserService->getProjections();

        $this->assertCount(2, $projections);
        $this->assertArrayHasKey('firstName', $projections[0]);
        $this->assertArrayHasKey('lastName', $projections[1]);
    }

    public function testGetMongoQuerySuccess() {
        $sqlParserService = new SQLParserService(self::SQL);
        $query = $sqlParserService->getMongoQuery();

        $result = [
            '$or' => [
                [
                    '$and' => [
                        ['firstName' =>['$eq' => 'Firstname']],
                        ['lastName' => ['$eq' => 'Lastname']]
                    ]
                ], [
                    '$and' => [
                        ['firstName' =>['$eq' => 'Firstname2']],
                        ['lastName' => ['$eq' => 'Lastname2']]
                    ]
                ]
            ]
        ];

        $this->assertEquals($result, $query);
    }

    public function testGetOrderSuccess() {
        $sqlParserService = new SQLParserService(self::SQL);
        $order = $sqlParserService->getOrder();

        $this->assertCount(1, $order);
        $this->assertArrayHasKey('firstName', $order);
        $this->assertEquals($order['firstName'], -1);
    }

    public function testGetSkipSuccess() {
        $sqlParserService = new SQLParserService(self::SQL);
        $skip = $sqlParserService->getOffset();

        $this->assertEquals($skip, 1);
    }

    public function testGetLimitSuccess() {
        $sqlParserService = new SQLParserService(self::SQL);
        $limit = $sqlParserService->getLimit();

        $this->assertEquals($limit, 20);
    }
}