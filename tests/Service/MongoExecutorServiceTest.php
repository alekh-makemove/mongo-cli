<?php
namespace Tests\Service;

use Tests\Base;
use MongoClient\Service\MongoExecutorService;
use Tests\FixtureService;

class MongoExecutorServiceTest extends Base {

    protected function setUp() {
        $this->createApplication();
        $fixtures = [
            [
                'name' => 'users',
                'data' => [
                    [
                        'firstName' => 'Firstname1',
                        'lastName' => 'Lastname1',
                    ], [
                        'firstName' => 'Firstname2',
                        'lastName' => 'Lastname2',
                    ], [
                        'firstName' => 'Firstname3',
                        'lastName' => 'Lastname3',
                    ]
                ]
            ]
        ];
        $fixtureController = new FixtureService($this->app['mongodb'], $this->app['testDbName']);
        $fixtureController->fixtures = $fixtures;
        $fixtureController->setFixtures();
    }

    public function testExecuteCommandByParametersSuccess() {
        $mongoExecutor = new MongoExecutorService($this->app['mongodb'], $this->app['testDbName']);

        $mongoQuery = [
            'firstName' => 'Firstname1',
            'lastName' => 'Lastname1'
        ];
        $collection = 'users';
        $projections = ['id' => 1, 'firstName' => 1, 'lastName' => 1];
        $order = ['firstName' => 1];
        $limit = 10;
        $offset = 0;

        $result = $mongoExecutor->executeCommandByParameters($mongoQuery, $collection, $projections, $order, $limit, $offset);
        // Get result as array
        $arrayResult = \iterator_to_array($result);

        foreach($arrayResult as $object) {
            $this->assertArrayHasKey('firstName', $object);
            $this->assertEquals($mongoQuery['firstName'], $object['firstName']);
            $this->assertArrayHasKey('lastName', $object);
            $this->assertEquals($mongoQuery['lastName'], $object['lastName']);
        }
    }
}